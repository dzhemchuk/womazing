<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $upsells ) : ?>
	<section class="new-collection section-padding">
		<div class="container">
			<h2 class="h2">
				Связанные товары
			</h2>
			<div class="triple-wrapper new-collection-wrapper">
				<?php woocommerce_product_loop_start(false); ?>
					<?php foreach ( $upsells as $upsell ) : ?>
						<a href="<?= get_permalink($upsell->id) ?>" class="triple-wrapper__block product-item">
							<div class="product-item__img">
								<img src="<?= wp_get_attachment_image_url($upsell->image_id, 'large') ?>" class="product-item-img">
							</div>
							<h4 class="product-item__title">
								<?= $upsell->name ?>
							</h4>
							<div class="product-item__price">
								<?php 
								if($upsell->regular_price != $upsell->price && !empty($upsell->price) && !empty($upsell->regular_price)){
								?>
								<span class="product-item-price product-item-price_old">
									<?= get_woocommerce_currency_symbol().$upsell->regular_price ?>
								</span>
								<?php 	
								}
								?>
								<span class="product-item-price">
									<?= get_woocommerce_currency_symbol().$upsell->price ?>
								</span>
							</div>
						</a>
					<?php endforeach; ?>
				<?php woocommerce_product_loop_end(false); ?>
			</div>
		</div>
	</section>

<?php
endif;

wp_reset_postdata();