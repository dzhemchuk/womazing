<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$available_variations = $product->get_available_variations();
$default_attrs = $product->get_default_attributes();

foreach ($available_variations as $available) {
	$def_quantity = count($default_attrs);
	$i = 0;
	foreach ($default_attrs as $def_name => $def_val) {
		if($available["attributes"]["attribute_".$def_name] == $def_val){
			$i++;
		}
	}

	if($i == $def_quantity){
		$def_id = $available["variation_id"];
	}
}
?>
<div class="shop-product-info__bottom">
	 

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );
	?>

	<input type="number" class="product-quantity" name="quantity" id="<?= uniqid( 'quantity_' ) ?>" value="<?= isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity() ?>" data-min="<?= $product->get_min_purchase_quantity() ?>" data-max="<?= $product->get_max_purchase_quantity() ?>">
	
	<?php
	do_action( 'woocommerce_after_add_to_cart_quantity' );
	do_action( 'woocommerce_before_add_to_cart_button' );
	?>

	<button type="submit" class="product-buy to-shop-btn">
		Добавить в корзину
	</button>

	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="<?= $def_id  ?>" />
</div>
