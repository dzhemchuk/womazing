<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 6.1.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

$attribute_keys  = array_keys( $attributes );
$variations_json = wp_json_encode( $available_variations );
$variations_attr = function_exists( 'wc_esc_json' ) ? wc_esc_json( $variations_json ) : _wp_specialchars( $variations_json, ENT_QUOTES, 'UTF-8', true );

do_action( 'woocommerce_before_add_to_cart_form' );

$c_default = $product->get_default_attributes();
$c_attributes = $product->get_attributes();
$default_attrs = $product->get_default_attributes();

foreach ($available_variations as $available) {
	$def_quantity = count($default_attrs);
	$i = 0;
	foreach ($default_attrs as $def_name => $def_val) {
		if($available["attributes"]["attribute_".$def_name] == $def_val){
			$i++;
		}
	}

	if($i == $def_quantity){
		$def_price = $available["display_regular_price"];
		$def_discount = $available["display_price"];
	}
}
?>

<div class="shop-product-info__price" data-symbol="<?= get_woocommerce_currency_symbol()?>">
	<span class="shop-product-info-price">
		<?= get_woocommerce_currency_symbol().$def_discount ?>
	</span>
	
	<span class="shop-product-info-price_old"<?php if($def_price == $def_discount || empty($def_discount)){ ?> style="display: none;"<?php } ?>>
		<?= get_woocommerce_currency_symbol().$def_price ?>
	</span>
</div>

<form class="shop-product-info" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo $variations_attr; // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

<?php
foreach ($c_attributes as $c_name => $c_value) {
	if($c_value->get_visible()){
	?>
	<div class="shop-product-info__option">
		<span class="shop-option-title">
			<?= get_taxonomy($c_name)->labels->singular_name ?>
		</span>
		<div class="shop-option-options">
			<?php
			$tmp_array = $c_value->get_options();
			asort($tmp_array);
			foreach ($tmp_array as $c_option) {
				$tmp_option = get_term_by( 'id', $c_option, $c_name );
			?>
			<div class="shop-option">			
				<input type="radio" id="<?= $c_value->get_name()."_".$tmp_option->slug ?>" value="<?= $tmp_option->slug ?>" name="<?= "attribute_".$c_value->get_name() ?>" class="shop-option__radio"<?php if($default_attrs[$c_value->get_name()] == $tmp_option->slug){ ?> checked="true"<?php } ?>>
				<?php if($c_value->get_name() == "pa_color"){ ?>
				<label class="shop-option__option shop-option__option_color" for="<?= $c_value->get_name()."_".$tmp_option->slug ?>" style="background: <?= $tmp_option->name; ?>"></label>
				<?php }else{ ?>
				<label class="shop-option__option" for="<?= $c_value->get_name()."_".$tmp_option->slug ?>">
					<?= $tmp_option->name; ?>
				</label>
				<?php } ?>
			</div>
			<?php 
			}
			?>
		</div>
	</div>
	<?php
	}
}
do_action( 'woocommerce_single_variation' );
?>