<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
	<div class="shop-product-info__price" data-symbol="<?= get_woocommerce_currency_symbol()?>">
		<span class="shop-product-info-price">
			<?= get_woocommerce_currency_symbol().$product->price ?>
		</span>
		
		<span class="shop-product-info-price_old"<?php if($product->price == $product->regular_price){ ?> style="display: none;"<?php } ?>>
			<?= get_woocommerce_currency_symbol().$product->regular_price ?>
		</span>
	</div>
	<form class="shop-product-info" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<div class="shop-product-info__bottom">
			<?php
			do_action( 'woocommerce_before_add_to_cart_quantity' );
			?>

			<input type="number" class="product-quantity" name="quantity" id="<?= uniqid( 'quantity_' ) ?>" value="<?= isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity() ?>" data-min="<?= $product->get_min_purchase_quantity() ?>" data-max="<?= $product->get_max_purchase_quantity() ?>">
			
			<?php
			do_action( 'woocommerce_after_add_to_cart_quantity' );
			?>

			<button type="submit" class="product-buy to-shop-btn" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
			Добавить в корзину
			</button>

			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
			</div>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
