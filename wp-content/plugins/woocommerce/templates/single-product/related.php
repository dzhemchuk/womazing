<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>
	<section class="new-collection section-padding">
		<div class="container">
			<h2 class="h2">
				Связанные товары
			</h2>
			<div class="triple-wrapper new-collection-wrapper">
				<?php woocommerce_product_loop_start(false); ?>
					<?php foreach ( $related_products as $related_product ) : ?>
						<a href="<?= get_permalink($related_product->id) ?>" class="triple-wrapper__block product-item">
							<div class="product-item__img">
								<img src="<?= wp_get_attachment_image_url($related_product->image_id, 'large') ?>" class="product-item-img">
							</div>
							<h4 class="product-item__title">
								<?= $related_product->name ?>
							</h4>
							<div class="product-item__price">
								<?php 
								if($related_product->regular_price != $related_product->price && !empty($related_product->price) && !empty($related_product->regular_price)){
								?>
								<span class="product-item-price product-item-price_old">
									<?= get_woocommerce_currency_symbol().$related_product->regular_price ?>
								</span>
								<?php 	
								}
								?>
								<span class="product-item-price">
									<?= get_woocommerce_currency_symbol().$related_product->price ?>
								</span>
							</div>
						</a>
					<?php endforeach; ?>
				<?php woocommerce_product_loop_end(false); ?>
			</div>
		</div>
	</section>

<?php
endif;

wp_reset_postdata();
