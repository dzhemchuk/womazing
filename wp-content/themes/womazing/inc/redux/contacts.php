<?php
/**
 * Redux Framework text config.
 * For full documentation, please visit: http://devs.redux.io/
 *
 * @package Redux Framework
 */

defined( 'ABSPATH' ) || exit;

Redux::set_section(
	$opt_name,
	array(
		'title'            => 'Контакты',
		'desc'             => 'Настройки страницы "Контакты"',
		'id'               => 'about-us',
		'subsection'       => true,
		'customizer_width' => '700px',
		'fields'           => array(
			array(
				'id'       => 'about-block-1',
				'type'     => 'section',
				'title'    => 'О нас блок 1',
				'indent'   => true,
			),
			array(
				'id'           => 'about-block-1__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'about-block-1__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'about-block-1__description-1',
				'type'     => 'textarea',
				'title'    => 'Описание 1',
			),
			array(
				'id'       => 'about-block-1__description-2',
				'type'     => 'textarea',
				'title'    => 'Описание 2',
			),
			array(
				'id'       => 'about-block-2',
				'type'     => 'section',
				'title'    => 'О нас блок 2',
				'indent'   => true,
			),
			array(
				'id'           => 'about-block-2__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'about-block-2__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'about-block-2__description-1',
				'type'     => 'textarea',
				'title'    => 'Описание 1',
			),
			array(
				'id'       => 'about-block-2__description-2',
				'type'     => 'textarea',
				'title'    => 'Описание 2',
			),
		),
	)
);
