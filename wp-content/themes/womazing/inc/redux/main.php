<?php
/**
 * Redux Framework text config.
 * For full documentation, please visit: http://devs.redux.io/
 *
 * @package Redux Framework
 */

defined( 'ABSPATH' ) || exit;

Redux::set_section(
	$opt_name,
	array(
		'title'            => 'Главная страница',
		'desc'             => 'Настройки главной страницы',
		'id'               => 'main-page',
		'subsection'       => true,
		'customizer_width' => '700px',
		'fields'           => array(
			array(
				'id'       => 'slider',
				'type'     => 'section',
				'title'    => 'Слайдер',
				'subtitle' => 'Слайдер на главной',
				'indent'   => true,
			),
			array(
				'id'           => 'slider-fly-1',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Плавающее фото 1',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'           => 'slider-fly-2',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Плавающее фото 2',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'slide-1',
				'type'     => 'section',
				'title'    => 'Слайд 1',
				'indent'   => true,
			),
			array(
				'id'           => 'slide-1__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'slide-1__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'slide-1__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'slide-2',
				'type'     => 'section',
				'title'    => 'Слайд 2',
				'indent'   => true,
			),
			array(
				'id'           => 'slide-2__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'slide-2__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'slide-2__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'slide-3',
				'type'     => 'section',
				'title'    => 'Слайд 3',
				'indent'   => true,
			),
			array(
				'id'           => 'slide-3__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'slide-3__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'slide-3__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'new-collection',
				'type'     => 'section',
				'title'    => 'Блок товаров на главной',
				'indent'   => true,
			),
			array(
				'id'       => 'new-collection__title',
				'type'     => 'text',
				'title'    => 'Заголовок секции',
			),
			array(
				'id'       => 'about',
				'type'     => 'section',
				'title'    => 'Что для нас важно',
				'indent'   => true,
			),
			array(
				'id'       => 'about__title',
				'type'     => 'text',
				'title'    => 'Заголовок секции',
			),
			array(
				'id'           => 'about-1__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Иконка',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'about-1__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'about-1__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'           => 'about-2__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Иконка',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'about-2__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'about-2__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'           => 'about-3__img',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Иконка',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'about-3__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'about-3__description',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'team',
				'type'     => 'section',
				'title'    => 'Команда',
				'indent'   => true,
			),
			array(
				'id'       => 'team__title',
				'type'     => 'text',
				'title'    => 'Заголовок секции',
			),
			array(
				'id'       => 'team-info__title',
				'type'     => 'text',
				'title'    => 'Заголовок',
			),
			array(
				'id'       => 'team-info__description-1',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'team-info__description-2',
				'type'     => 'textarea',
				'title'    => 'Описание',
			),
			array(
				'id'       => 'team-info__link',
				'type'     => 'text',
				'title'    => 'Текст ссылки на страницу о бренде',
			),
			array(
				'id'           => 'team-info__slide-1',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото слайдера 1',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'           => 'team-info__slide-2',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото слайдера 2',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'           => 'team-info__slide-3',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Фото слайдера 3',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
		),
	)
);
