<?php
/**
 * Redux Framework text config.
 * For full documentation, please visit: http://devs.redux.io/
 *
 * @package Redux Framework
 */

defined( 'ABSPATH' ) || exit;

Redux::set_section(
	$opt_name,
	array(
		'title'            => 'Основные настройки',
		'desc'             => 'Основные настройки сайта',
		'id'               => 'general-settings',
		'subsection'       => true,
		'customizer_width' => '700px',
		'fields'           => array(
			array(
				'id'           => 'main__logo',
				'type'         => 'media',
				'url'          => true,
				'title'        => 'Логотип',
				'compiler'     => 'true',
				'preview_size' => 'full',
			),
			array(
				'id'       => 'main__phone',
				'type'     => 'text',
				'title'    => 'Номер телефона',
			),
			array(
				'id'       => 'main__email',
				'type'     => 'text',
				'title'    => 'Почта',
			),
			array(
				'id'       => 'main__address',
				'type'     => 'text',
				'title'    => 'Адрес',
			),
			array(
				'id'       => 'main__map',
				'type'     => 'textarea',
				'title'    => 'Код карты',
			),
			array(
				'id'       => 'instagram',
				'type'     => 'text',
				'title'    => 'Ссылка на Instagram',
			),
			array(
				'id'       => 'facebook',
				'type'     => 'text',
				'title'    => 'Ссылка на Facebook',
			),
			array(
				'id'       => 'twitter',
				'type'     => 'text',
				'title'    => 'Ссылка на Twitter',
			),
		),
	)
);
