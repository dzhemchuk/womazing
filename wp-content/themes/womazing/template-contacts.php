<?php
/* Template Name: Contacts */
get_header();
?>
	<section class="big-padding">
		<div class="container">
			<h1 class="h1 page-h1">
				<?= get_the_title() ?>
			</h1>
			<?php include('breadcrumbs.php'); ?>
			<div class="contacts-block about-wrapper">
				<div class="contacts-block__map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d72902.26494620056!2d-79.49023690797947!3d43.71800466551593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d4cb90d7c63ba5%3A0x323555502ab4c477!2sToronto%2C%20ON%2C%20Canada!5e0!3m2!1sen!2sua!4v1648783042435!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
				</div>
				<div class="contacts-block__contacts">
					<div class="contact-block">
						<span class="contact-block__title">
							Телефон
						</span>
						<?php 
						$clear_phone=str_replace(' ', '', $redux['main__phone']);
						$clear_phone=str_replace('(', '', $clear_phone);
						$clear_phone=str_replace(')', '', $clear_phone);
						$clear_phone=str_replace('-', '', $clear_phone);
						?>
						<a href="tel:<?= $clear_phone ?>" class="contact-block__link">
							<?= $redux['main__phone'] ?>
						</a>
					</div>
					<div class="contact-block">
						<span class="contact-block__title">
							E-mail
						</span>
						<a href="mailto:<?= $redux['main__email'] ?>" class="contact-block__link">
							<?= $redux['main__email'] ?>
						</a>
					</div>
					<div class="contact-block">
						<span class="contact-block__title">
							Адрес
						</span>
						<span class="contact-block__link">
							<?= $redux['main__address'] ?>
						</span>
					</div>
				</div>
				<form action="<?= home_url() ?>/mail.php" id="contacts-form" class="contacts-block__feedback">
					<h3 class="h3">
						Напишите нам
					</h3>
					<div class="feedback-form">
						<input type="hidden" name="mailto" class="universal-input" value="<?= $redux['main__email'] ?>">
						<input type="hidden" name="type" class="universal-input" value="contacts">
						<input type="text" name="name" class="universal-input" placeholder="Имя">
						<input type="email" name="email" class="universal-input" placeholder="E-mail">
						<input type="text" name="phone" class="universal-input" placeholder="Телефон">
						<textarea class="universal-textarea" name="message" placeholder="Сообщение"></textarea>
					</div>
					<button type="submit" class="to-shop-btn">
						Отправить
					</button>
					<p class="contacts-block__message">
						Сообщение успешно отправлено
					</p>
				</form>
				
			</div>
		</div>
	</section>
<?php
get_footer();
?>