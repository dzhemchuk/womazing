<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package womazing
 */
GLOBAL $redux;
?>
	<footer class="footer">
		<div class="container">
			<div class="footer-wrapper">
				<div class="footer-left">
					<a href="<?= home_url() ?>" class="logo">
						<img src="<?= $redux['main__logo']['url'] ?>" class="logo__img">
					</a>
					<div class="copyright">
						<p class="copyright__right">
							© Все права защищены
						</p>
						<?php 

						$menuParameters = array(
						  'menu'       => 18,
						  'container'       => false,
						  'echo'            => false,
						  'items_wrap'      => '%3$s',
						  'depth'           => 0,
						  'link_class'  => 'copyright__link'
						);

						echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
						 ?>
					</div>
				</div>
				<div class="footer-center">
					<ul class="footer-link-block">
						<?php 
							$footer_menu = wp_get_nav_menu_items(17);
							foreach ($footer_menu as $menu) {
						?>
						<li class="footer-link-block__li">
							<a href="<?= $menu->url ?>" class="nav__link<?php if(is_page($menu->object_id)){ ?> nav__link_active<?php } ?>"> 
								<?= $menu->title ?>
							</a>
							<?php
							if($menu->object_id == 17){
							?>
							<ul class="footer-link-sublock">
								<?php
								$categories = get_categories( array('taxonomy' => 'product_cat', 'hide_empty' => 0) );
								foreach ($categories as $category) {
								?>
									<li class="footer-link-sublock__li">
										<a href="<?= get_category_link($category->cat_ID) ?>" class="footer-link-list">
											<?= $category->name ?>
										</a>
									</li>
								<?php
								}
								?>
							</ul>
							<?php	
							}
							?>
						</li>
						<?php 
							}
						 ?>
					</ul>
				</div>
				<div class="footer-right">
					<div class="footer-contants">
						<?php 
						$clear_phone=str_replace(' ', '', $redux['main__phone']);
						$clear_phone=str_replace('(', '', $clear_phone);
						$clear_phone=str_replace(')', '', $clear_phone);
						$clear_phone=str_replace('-', '', $clear_phone);
						?>
						<a href="tel:<?= $clear_phone ?>" class="phone footer-contants__contact">
							<?= $redux['main__phone'] ?>
						</a>
						<a href="mailto:<?= $redux['main__email'] ?>" class="phone footer-contants__contact">
							<?= $redux['main__email'] ?>
						</a>
					</div>
					<div class="footer-socials">
						<?php if(!empty($redux['instagram'])){ ?>
						<a href="<?= $redux['instagram'] ?>" class="footer-socials__social">
							<img src="<?= get_template_directory_uri() ?>/img/socials/instagram.svg" class="footer-socials-img">
						</a>
						<?php } ?>
						<?php if(!empty($redux['facebook'])){ ?>
						<a href="<?= $redux['facebook'] ?>" class="footer-socials__social">
							<img src="<?= get_template_directory_uri() ?>/img/socials/facebook.svg" class="footer-socials-img">
						</a>
						<?php } ?>
						<?php if(!empty($redux['twitter'])){ ?>
						<a href="<?= $redux['twitter'] ?>" class="footer-socials__social">
							<img src="<?= get_template_directory_uri() ?>/img/socials/twitter.svg" class="footer-socials-img">
						</a>
						<?php } ?>
					</div>
					<div class="footer-payments">
						<img src="<?= get_template_directory_uri() ?>/img/visa-master.png" class="footer-payments__system">
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal">
		<div class="modal-bg modal-btn_close"></div>
		<div class="modal-wrapper">
			<button type="button" class="modal-close modal-btn_close">
				<img src="<?= get_template_directory_uri() ?>/img/icons/close.svg" class="modal-close__img">
			</button>
			<div class="modal-window modal-window_callback">
				<h3 class="h3 modal-title">
					Заказать обратный звонок
				</h3>
				<form action="<?= home_url() ?>/mail.php" id="modal-form">
					<input type="hidden" name="mailto" class="universal-input" value="<?= $redux['main__email'] ?>">
					<input type="hidden" name="type" class="universal-input" value="modal">
					<input type="text" name="name" class="universal-input" placeholder="Имя">
					<input type="email" name="email" class="universal-input" placeholder="E-mail">
					<input type="text" name="phone" class="universal-input" placeholder="Телефон">
					<button type="submit" class="to-shop-btn modal-btn">
						Заказать звонок
					</button>
				</form>
			</div>
			<div class="modal-window modal-window_success">
				<h3 class="h3 modal-title">
					Отлично! Мы скоро вам перезвоним.
				</h3>
				<button type="button" class="to-shop-btn to-shop-btn_white modal-btn-success modal-btn_close">
					Закрыть
				</button>
			</div>
		</div>
	</div>

	<?php wp_footer(); ?>

	<?php if(is_product()){ ?>
	<script>
		$(".navigation.post-navigation").hide();
	</script>
	<?php } ?>

	<?php if(is_front_page()){ ?>
	<script>
		var offer = $('.slider-offer');

		offer.owlCarousel({
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			smartSpeed: 500,
		    loop:false,
		    items: 1,
		    mouseDrag: false,
		    touchDrag: false,
		    pullDrag: false,
		    autoplay: true,
		    dotsContainer: '.hero-slider-nav'
		});

		var photo = $('.hero-photo-slider');

		photo.owlCarousel({
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			smartSpeed: 500,
		    loop:false,
		    items: 1,
		    mouseDrag: false,
		    dots: false,
		    touchDrag: false,
		    pullDrag: false,
		    autoplay: true
		});

		$('.hero-slider-nav__btn').click(function () {
		  	offer.trigger('to.owl.carousel', [$(this).index(), 300]);
		  	photo.trigger('to.owl.carousel', [$(this).index(), 300]);
		});

		var team = $('.team-slider');

		team.owlCarousel({
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			smartSpeed: 500,
		    loop:true,
		    items: 1,
		    mouseDrag: false,
		    dots: true,
		    touchDrag: false,
		    pullDrag: false,
		    autoplay: true,
		    dotsContainer: '.team-slider-dots'
		});

		$('.team-slider-dots__dot').click(function () {
		  	team.trigger('to.owl.carousel', [$(this).index(), 300]);
		});

		$('.team-slider-btn_prev').click(function() {
		    team.trigger('prev.owl.carousel');
		})

		$('.team-slider-btn_next').click(function() {
		    team.trigger('next.owl.carousel');
		})
	</script>
	<?php } ?>
</body>
</html>