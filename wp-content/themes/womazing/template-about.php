<?php
/* Template Name: About us */
get_header();
?>
	<section class="big-padding">
		<div class="container">
			<h1 class="h1 page-h1">
				<?= get_the_title() ?>
			</h1>
			<?php include('breadcrumbs.php'); ?>
			<div class="about-wrapper">
				<div class="about-block">
					<div class="about-block__img">
						<img src="<?= $redux['about-block-1__img']['url'] ?>" class="about-block-img">
					</div>
					<div class="about-block__story">
						<div class="about-block-wrapper">
							<h3 class="h3">
								<?= $redux['about-block-1__title'] ?>
							</h3>
							<?php if($redux['about-block-1__description-1']){ ?>
							<p class="about-block-p">
								<?= $redux['about-block-1__description-1'] ?>
							</p>
							<?php } ?>
							<?php if($redux['about-block-1__description-2']){ ?>
							<p class="about-block-p">
								<?= $redux['about-block-1__description-2'] ?>
							</p>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="about-block">
					<div class="about-block__story">
						<div class="about-block-wrapper">
							<h3 class="h3">
								<?= $redux['about-block-2__title'] ?>
							</h3>
							<?php if($redux['about-block-2__description-1']){ ?>
							<p class="about-block-p">
								<?= $redux['about-block-2__description-1'] ?>
							</p>
							<?php } ?>
							<?php if($redux['about-block-2__description-2']){ ?>
							<p class="about-block-p">
								<?= $redux['about-block-2__description-2'] ?>
							</p>
							<?php } ?>
						</div>
					</div>
					<div class="about-block__img">
						<img src="<?= $redux['about-block-2__img']['url'] ?>" class="about-block-img">
					</div>
				</div>
			</div>
			<a href="<?=  get_permalink( wc_get_page_id( 'shop' ) ) ?>" class="to-shop-btn about-btn">
				Открыть магазин
			</a>
		</div>
	</section>
<?php
get_footer();
?>