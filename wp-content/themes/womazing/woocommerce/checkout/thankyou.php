<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<section class="big-padding">
	<div class="container">
		<h1 class="h1 page-h1">
			Заказ получен
		</h1>
		<?php include(get_template_directory().'/breadcrumbs.php'); ?>
		<div class="done about-wrapper">
			<div class="done-info">
				<div class="done-info__img">
					<img src="<?= get_template_directory_uri() ?>/img/icons/done.svg">
				</div>
				<div class="done-info__text">
					<h3 class="h3">
						Заказ успешно оформлен
					</h3>
					<p class="done-text">
						Мы свяжемся с вами в ближайшее время!
					</p>
				</div>
			</div>
			<a href="<?= home_url() ?>" class="to-shop-btn to-shop-btn_white">
				Перейти на главную
			</a>
		</div>
	</div>
</section>
