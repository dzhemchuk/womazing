<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<section class="big-padding">
	<div class="container">
		<h1 class="h1 page-h1">
			Оформление заказа
		</h1>
		<?php include(get_template_directory().'/breadcrumbs.php'); ?>
		<form class="checkout about-wrapper woocommerce-checkout" name="checkout" method="post" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
			<?php if ( $checkout->get_checkout_fields() ) : ?>

			<div class="checkout-right">
				<div class="checkout-form-block">
					<h3 class="h3">
						Данные покупателя
					</h3>
					<div class="checkout-fields">
						<?php 
							$cfields = $checkout->get_checkout_fields();

							foreach ($cfields["billing"] as $key => $cfield) {
							?>

							<input type="<?= $cfield["type"] ?>" class="universal-input" placeholder="<?= $cfield["label"] ?>" name="<?= $key ?>"<?php if($cfield["required"]){ ?> required <?php } ?>>
							
							<?php
							}
						?>
					</div>
				</div>
				<div class="checkout-form-block">
					<h3 class="h3">
						Адрес получателя
					</h3>
					<div class="checkout-fields">
						<?php 
							foreach ($cfields["shipping"] as $key => $cfield) {
							?>

							<input type="<?= $cfield["type"] ?>" class="universal-input" placeholder="<?= $cfield["label"] ?>" name="<?= $key ?>"<?php if($cfield["required"]){ ?> required <?php } ?>>
							
							<?php
							}
						?>
					</div>
				</div>
				<div class="checkout-form-block">
					<h3 class="h3">
						Комментарии
					</h3>
					<div class="checkout-fields">
						<?php 
							foreach ($cfields["order"] as $key => $cfield) {
							?>

							<textarea class="universal-textarea" placeholder="<?= $cfield["label"] ?>" name="<?= $key ?>"<?php if($cfield["required"]){ ?> required <?php } ?>></textarea>
							
							<?php
							}
						?>
						
					</div>
				</div>
			</div>
			<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			<?php endif; ?>
		</form>			
	</div>
</section>

