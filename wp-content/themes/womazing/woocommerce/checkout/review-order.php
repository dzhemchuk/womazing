<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="checkout-left">
	<div class="checkout-form-block checkout-form-block_big">
		<h3 class="h3 checkout-padding payment-selector">
			Ваш заказ
		</h3>
		<div class="checkout-fields">
			<div class="total-row total-row_title">
				<span class="total-row__title">
					Товар
				</span>
				<span class="total-row__value">
					Всего
				</span>
			</div>
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					?>

					<div class="total-row">
						<span class="total-row__title">
							<?= $_product->get_name() ?>
						</span>
						<span class="total-row__value">
							<?= get_woocommerce_currency_symbol().($_product->price*$cart_item['quantity']) ?>
						</span>
					</div>

					<?php
				}
			}
			?>
			<div class="total-row">
				<span class="total-row__title">
					Подытог
				</span>
				<span class="total-row__value">
					<?= get_woocommerce_currency_symbol().WC()->cart->get_subtotal() ?>
				</span>
			</div>

			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>

				<div class="total-row">
					<span class="total-row__title">
						Купон
					</span>
					<span class="total-row__value">
						– <?= get_woocommerce_currency_symbol().$coupon->amount ?>
					</span>
				</div>

			<?php endforeach; ?>

			<div class="total-row total-row_bg">
				<span class="total-row__title">
					Итого
				</span>
				<span class="total-row__value">
					<?= get_woocommerce_currency_symbol().WC()->cart->cart_contents_total ?>
				</span>
			</div>
		</div>
	</div>
