<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>
	<section class="big-padding">
		<div class="container">
			<h1 class="h1 page-h1">
				Корзина
			</h1>
			<?php include(get_template_directory().'/breadcrumbs.php'); ?>
			<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
			<div class="cart about-wrapper">
				<div class="cart-row cart-title">
					<span class="cart-col-1">
						Товар
					</span>
					<span class="cart-col-2">
						Цена
					</span>
					<span class="cart-col-3">
						Количество
					</span>
					<span class="cart-col-4">
						Всего
					</span>
				</div>
				<?php
				foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
						?>

						<div class="cart-row">
							<div class="cart-col-1">
								<div class="cart-col-wrapper">
									<a href="<?=  wc_get_cart_remove_url( $cart_item_key ) ?>" class="cart-remove">
										<img src="<?= get_template_directory_uri() ?>/img/icons/close.svg" class="cart-remove__img">
									</a>
									<a href="<?= get_permalink($_product->id) ?>" class="cart-item">
										<div class="cart-item-img">
											<img src="<?= wp_get_attachment_image_url($_product->image_id, 'medium') ?>" class="cart-item-img__img">
										</div>
										<span class="cart-item-name">
											<?= $_product->get_name() ?>
										</span>
									</a>
								</div>
							</div>
							<span class="cart-col-2 cart-text-all">
								<?= get_woocommerce_currency_symbol().$_product->price ?>
							</span>
							<div class="cart-col-3">
								<input type="number" class="product-quantity" name="cart[<?= $cart_item_key ?>][qty]" value="<?= $cart_item['quantity'] ?>" data-min="1" data-max="<?= $_product->get_max_purchase_quantity() ?>">
							</div>
							<span class="cart-col-4 cart-text-all">
								<?= get_woocommerce_currency_symbol().($_product->price*$cart_item['quantity']) ?>
							</span>
						</div>
						<?php
					}
				}
				?>
			</div>
			<div class="after-cart">
				<?php if ( wc_coupons_enabled() ) { ?>
					<div class="after-cart__wrapper">
						<input type="text" class="universal-input after-cart-input" placeholder="Введите купон" id="coupon_code" value=""> 
						<button type="button" id="apply_coupon" class="to-shop-btn to-shop-btn_white after-cart-btn">
							Применить купон
						</button>

						<?php do_action( 'woocommerce_cart_coupon' ); ?>
					</div>
				<?php } ?>
				<button type="submit" class="to-shop-btn to-shop-btn_white" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>">
					Обновить корзину
				</button>
			</div>
			<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
			</form>
			
			<?php do_action( 'woocommerce_cart_collaterals' ); ?>
		</div>
	</section>

