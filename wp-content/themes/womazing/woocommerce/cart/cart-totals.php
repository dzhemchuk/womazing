<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.3.6
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="cart-total">
	<span class="cart-total__title">
		Подытог: <?php wc_cart_totals_subtotal_html(); ?>
	</span>

	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
	<span class="cart-total__title">
		<?php wc_cart_totals_coupon_label( $coupon ); ?> <?php wc_cart_totals_coupon_html( $coupon ); ?>
	</span>
	<?php endforeach; ?>
	<div class="cart-total__wrapper">
		<div class="cart-total-in-total">
			<span class="cart-total-in-total__title">
				Итого:
			</span>
			<span class="cart-total-in-total__amount">
				<?= get_woocommerce_currency_symbol().WC()->cart->total ?>
			</span>
		</div>
		<a href="<?= wc_get_checkout_url() ?>" class="to-shop-btn cart-submit">
			Оформить заказ
		</a>
	</div>
</div>