<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<section class="big-padding">
	<div class="container">
		<h1 class="h1 page-h1">
			Магазин
		</h1>
		<?php include(get_template_directory().'/breadcrumbs.php'); ?>
		<div class="shop-categories">
			<a href="<?= get_permalink( wc_get_page_id( 'shop' ) ) ?>" class="shop-categories__category<?php if(is_shop()){ ?> shop-categories__category_active<?php } ?>">
				
				Все
			</a>
			<?php
			$categories = get_categories( array('taxonomy' => 'product_cat', 'hide_empty' => 0) );
			foreach ($categories as $category) {
			?>
				<a href="<?= get_category_link($category->cat_ID) ?>" class="shop-categories__category<?php if(is_product_category($category->cat_ID)){ ?> shop-categories__category_active<?php } ?>">
					<?= $category->name ?>
				</a>
			<?php
			}
			?>
		</div>
		<div class="shop-content">
			<?php
				if ( woocommerce_product_loop() ) {

					woocommerce_product_loop_start();

					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
						}
					}

					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}
			?>
		</div>
	</div>

</section>
<?php 
get_footer( 'shop' );
 ?>