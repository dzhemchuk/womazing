<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>


<!-- Карточка товара -->
<a href="<?= get_permalink($product->id) ?>" class="triple-wrapper__block product-item">
	<div class="product-item__img">
		<img src="<?= wp_get_attachment_image_url($product->image_id, 'large') ?>" class="product-item-img">
	</div>
	<h4 class="product-item__title">
		<?= $product->name ?>
	</h4>
	<div class="product-item__price">
		<?php 
		if($product->regular_price != $product->price && !empty($product->price) && !empty($product->regular_price)){
		?>
		<span class="product-item-price product-item-price_old">
			<?= get_woocommerce_currency_symbol().$product->regular_price ?>
		</span>
		<?php 	
		}
		?>
		<span class="product-item-price">
			<?= get_woocommerce_currency_symbol().$product->price ?>
		</span>
	</div>
</a>