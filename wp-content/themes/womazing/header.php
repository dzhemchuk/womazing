<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package womazing
 */
GLOBAL $redux;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- OG Tags -->
	<meta property="og:title" content="<?=  wp_get_document_title() ?>">
	<meta property="og:type" content="article">
	<meta property="og:image" content="<?= site_url() ?>/wp-content/themes/womazing/screenshot.png">
	<meta property="og:site_name" content="<?=  wp_get_document_title() ?>">

	<!-- Favicons -->
	<meta name="theme-color" content="#e7ecf4">
	<link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/img/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/img/favicon.png">

	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com"> 
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> 
	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;700&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body>
	<?php if(is_front_page()){ ?>
	<div class="header-wrapper">
	<?php } ?>
		<header class="header">
			<div class="container">
				<div class="header-panel">
					<button type="button" class="mobile-btn">
						<img src="<?= get_template_directory_uri() ?>/img/icons/menu.svg" class="mobile-btn__img">
					</button>
					<a href="<?= home_url() ?>" class="logo">
						<img src="<?= $redux['main__logo']['url'] ?>" class="logo__img">
					</a>
					<nav class="nav nav_mobile">
						<button type="button" class="mobile-close">
							<img src="<?= get_template_directory_uri() ?>/img/icons/close.svg" class="mobile-close__img">
						</button>
						<?php 

						$menuParameters = array(
						  'container'       => false,
						  'echo'            => false,
						  'items_wrap'      => '%3$s',
						  'depth'           => 0,
						  'link_class'  => 'nav__link'
						);

						echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
						 ?>
					</nav>
					<div class="header-right">
						<?php 
						$clear_phone=str_replace(' ', '', $redux['main__phone']);
						$clear_phone=str_replace('(', '', $clear_phone);
						$clear_phone=str_replace(')', '', $clear_phone);
						$clear_phone=str_replace('-', '', $clear_phone);
						?>
						<a href="tel:<?= $clear_phone ?>" class="phone phone_icon">
							<?= $redux['main__phone'] ?>
						</a>
						<a href="<?= wc_get_cart_url() ?>" class="mini-cart">
							<img src="<?= get_template_directory_uri() ?>/img/icons/cart.svg" class="mini-cart__icon">

							<span class="mini-cart__quantity<?php if(WC()->cart->get_cart_contents_count() == "0"){ ?> mini-cart__quantity_hidden<?php } ?>">
								<?= WC()->cart->get_cart_contents_count() ?>
							</span>
						</a>
					</div>
				</div>
			</div>
		</header>
	<?php if(is_front_page()){ ?>
		<section class="hero">
			<div class="container">
				<div class="hero-wrapper">
					<div class="hero-offer-wrapper">
						<div class="hero-offer">
							<div class="slider-offer owl-carousel">
								<div class="slider-offer__slide">
									<h1 class="h1">
										<?= $redux['slide-1__title'] ?>
									</h1>
									<p class="p-offer">
										<?= $redux['slide-1__description'] ?>
									</p>
								</div>
								<div class="slider-offer__slide">
									<h1 class="h1">
										<?= $redux['slide-2__title'] ?>
									</h1>
									<p class="p-offer">
										<?= $redux['slide-2__description'] ?>
									</p>
								</div>
								<div class="slider-offer__slide">
									<h1 class="h1">
										<?= $redux['slide-3__title'] ?>
									</h1>
									<p class="p-offer">
										<?= $redux['slide-3__description'] ?>
									</p>
								</div>
							</div>
							<div class="hero-offer__btns">
								<button type="button" class="hero-offer-scroll"></button>
								<a href="<?=  get_permalink( wc_get_page_id( 'shop' ) ) ?>" class="to-shop-btn">
									Открыть магазин
								</a>
							</div>
						</div>
						<div class="hero-slider-nav">
							<button type="button" class="hero-slider-nav__btn"></button>
							<button type="button" class="hero-slider-nav__btn"></button>
							<button type="button" class="hero-slider-nav__btn"></button>
						</div>
					</div>
					<div class="hero-photo">
						<div class="hero-photo-fly hero-photo-fly_1">
							<img src="<?= $redux['slider-fly-1']['url'] ?>" class="hero-photo-fly__img">
						</div>
						<div class="hero-photo-slider owl-carousel">
							<div class="hero-photo-slider__slide">
								<img src="<?= $redux['slide-1__img']['url'] ?>" class="hero-photo-slider-img">
							</div>
							<div class="hero-photo-slider__slide">
								<img src="<?= $redux['slide-2__img']['url'] ?>" class="hero-photo-slider-img">
							</div>
							<div class="hero-photo-slider__slide">
								<img src="<?= $redux['slide-3__img']['url'] ?>" class="hero-photo-slider-img">
							</div>
						</div>
						<div class="hero-photo-fly hero-photo-fly_2">
							<img src="<?= $redux['slider-fly-2']['url'] ?>" class="hero-photo-fly__img">
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php } ?>
