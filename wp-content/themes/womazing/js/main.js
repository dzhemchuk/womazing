$(document).ready(function(){
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	    if (scroll >= 500) {
	        $(".header").addClass("header_sticky");
	    }
	    else{
	    	$(".header").removeClass("header_sticky");
	    }
	});

	$(".mobile-btn").click(function(){
		$(".nav").slideDown();
	});

	$(".mobile-close").click(function(){
		$(".nav").slideUp();
	});

	$(".hero-offer-scroll").click(function(){
		$('html, body').animate({
            scrollTop: $(".new-collection .h2").offset().top
        }, 2000);
	});

	$(".header-panel .phone").click(function(){
		$(".modal").addClass("modal_active");
		$(".modal-window_callback").addClass("modal-window_active");
		return false;
	});

	$(".modal-btn_close").click(function(){
		$(".modal").removeClass("modal_active");
		$(".modal-window_callback").removeClass("modal-window_active");
	});

	$(".shop-pagination__btn_active").click(function(){
		return false;
	});

	$(".product-quantity").change(function(){
		if($(this).val()<1){
			$(this).val("1");
		}

		if($(this).data("max") > 0){
			if($(this).val() > $(this).data("max")){
				$(this).val( $(this).data("max") );
			}
		}
	});

	$.validator.addMethod("regex", function(value, element, regexp) {
	        var regExsp = new RegExp(regexp);
	        return regExsp.test(value);
	    },"Please check your input."
	);

  	$("#modal-form").validate({
      	rules: {
          	name: {
				required: true,
				regex : "[A-Za-z]{1,32}"   
          	},
          	email: {
				required: true,
				email: true 
          	},
          	phone: {
				digits : true,
				required: true,
				minlength: 10,
				maxlength: 11,
				regex: "[0-9]+"
          	}
      	},
      	submitHandler: function(form){
      		event.preventDefault();

	        $.ajax({
				url: $(form).attr("action"),
				method: "POST",
				data: $(form).serialize(),
				success: function(data){
					if(data=="OK"){
						alert("Запрос успешно отправлен! Ожидайте звонка менеджера.");
						location.reload();
					}
					else{
					  	alert("Произошла ошибка при отправке запроса");
					}
				},
				error: function(){
					alert("Произошла ошибка при отправке запроса");
				}
	        });
      }
  });

  	$("#contacts-form").validate({
      	rules: {
          	name: {
				required: true,
				regex : "[A-Za-z]{1,32}"   
          	},
          	email: {
				required: true,
				email: true 
          	},
          	message: {
				required: true
          	},
          	phone: {
				digits : true,
				required: true,
				minlength: 10,
				maxlength: 11,
				regex: "[0-9]+"
          	}
      	},
      	submitHandler: function(form){
      		event.preventDefault();

	        $.ajax({
				url: $(form).attr("action"),
				method: "POST",
				data: $(form).serialize(),
				success: function(data){
					if(data=="OK"){
						$(".contacts-block__message").show();
					}
					else{
					  	alert("Произошла ошибка при отправке запроса");
					}
				},
				error: function(){
					alert("Произошла ошибка при отправке запроса");
				}
	        });
      	}
  	});

  	$(".shop-option__option").click(function(){
  		$("input[name='"+$(this).siblings("input").attr("name")+"']").prop( "checked", false );
  		$(this).siblings("input").prop( "checked", true );


  		let obj =  JSON.stringify( $(this).parents("form.shop-product-info").data("product_variations") );
  		let product_variations = jQuery.parseJSON(obj);

  		let product_attrs = {};
  		$(".shop-option__radio:checked").each(function(){
  			product_attrs[$(this).attr("name")] = $(this).val();
  		});
  		console.log(product_attrs);

  		let attr_quantity = Object.keys(product_attrs).length;

		$.each(product_variations, function( index, value ) {
			j = 0;

			$.each(value.attributes, function( index, key ) {

			  	if(product_attrs[index] == key){
			  		j++;
			  	}
			  	
			if(attr_quantity == j){
				price = value.display_price;
				price_regular = value.display_regular_price;
				$("input[name='variation_id']").val(value.variation_id);
				return false;
			}
			});
		});

		if(price != price_regular){
			$(".shop-product-info-price").html($(".shop-product-info__price").data("symbol") + price);
			$(".shop-product-info-price_old").html($(".shop-product-info__price").data("symbol") + price_regular);
			$(".shop-product-info-price_old").show();
		}
		else{
			$(".shop-product-info-price_old").hide();
			$(".shop-product-info-price").html($(".shop-product-info__price").data("symbol") + price_regular);
		}
  		
  	});

  	$(".shop-product-info").submit(function(e){
  		alert("Добавлено в корзину");
  	});


  	$("#apply_coupon").click(function(){
  		//wc_cart_params.apply_coupon_nonce
        $.ajax({
			url: "/?wc-ajax=apply_coupon",
			method: "POST",
			data: "security="+wc_cart_params.apply_coupon_nonce+"&coupon_code="+$("#coupon_code").val(),
			success: function(data){
				let answer = $(data).text().replace(/\s+/g, " ");
				alert(answer);
				if(answer == " Код купона успешно добавлен. "){
					location.reload();
				}
			},
			error: function(){
				alert("Произошла ошибка при отправке запроса");
			}
        });
  	});
});