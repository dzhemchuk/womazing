<?php
/* Template Name: Main Page */
get_header();
?>
	<?php 
	$args = array(
	    'tag' => array('new-collection'),
	    'limit' => 3
	);

	$products = wc_get_products($args);
	if(count($products)>0){
	?>
	<section class="new-collection section-padding">
		<div class="container">
			<h2 class="h2">
				<?= $redux['new-collection__title'] ?>
			</h2>
			<div class="triple-wrapper new-collection-wrapper">
				<?php 
				foreach ($products as $product) {
				?>
				<a href="<?= get_permalink($product->id) ?>" class="triple-wrapper__block product-item">
					<div class="product-item__img">
						<img src="<?= wp_get_attachment_image_url($product->image_id, 'large') ?>" class="product-item-img">
					</div>
					<h4 class="product-item__title">
						<?= $product->name ?>
					</h4>
					<div class="product-item__price">
						<?php 
						if($product->regular_price != $product->price && !empty($product->price) && !empty($product->regular_price)){
						?>
						<span class="product-item-price product-item-price_old">
							<?= get_woocommerce_currency_symbol().$product->regular_price ?>
						</span>
						<?php 	
						}
						?>
						<span class="product-item-price">
							<?= get_woocommerce_currency_symbol().$product->price ?>
						</span>
					</div>
				</a>
				<?php
				}
				?>
			</div>
			<a href="<?=  get_permalink( wc_get_page_id( 'shop' ) ) ?>" class="to-shop-btn to-shop-btn_white new-collection-btn">
				Открыть магазин
			</a>
		</div>
	</section>
	<?php 
	}
	?>
	<section class="about-us section-padding">
		<div class="container">
			<h2 class="h2">
				<?= $redux['about__title'] ?>
			</h2>
			<div class="triple-wrapper about-us-wrapper">
				<div class="triple-wrapper__block reason">
					<div class="reason__icon">
						<img src="<?= $redux['about-1__img']['url'] ?>" class="reason-icon-img">
					</div>
					<h3 class="h3 reason__h3">
						<?= $redux['about-1__title'] ?>
					</h3>
					<p class="reason__p">
						<?= $redux['about-1__description'] ?>
					</p>
				</div>
				<div class="triple-wrapper__block reason">
					<div class="reason__icon">
						<img src="<?= $redux['about-2__img']['url'] ?>" class="reason-icon-img">
					</div>
					<h3 class="h3 reason__h3">
						<?= $redux['about-2__title'] ?>
					</h3>
					<p class="reason__p">
						<?= $redux['about-2__description'] ?>
					</p>
				</div>
				<div class="triple-wrapper__block reason">
					<div class="reason__icon">
						<img src="<?= $redux['about-3__img']['url'] ?>" class="reason-icon-img">
					</div>
					<h3 class="h3 reason__h3">
						<?= $redux['about-3__title'] ?>
					</h3>
					<p class="reason__p">
						<?= $redux['about-3__description'] ?>
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="section-padding">
		<div class="container">
			<h2 class="h2">
				<?= $redux['team__title'] ?>
			</h2>
			<div class="our-team">
				<div class="our-team__slider">
					<div class="team-slider owl-carousel">
						<div class="team-slider__slide">
							<img src="<?= $redux['team-info__slide-1']['url'] ?>" class="team-slider-img">
						</div>
						<div class="team-slider__slide">
							<img src="<?= $redux['team-info__slide-2']['url'] ?>" class="team-slider-img">
						</div>
						<div class="team-slider__slide">
							<img src="<?= $redux['team-info__slide-3']['url'] ?>" class="team-slider-img">
						</div>
					</div>
					<div class="team-slider-dots">
						<button type="button" class="team-slider-dots__dot active"></button>
						<button type="button" class="team-slider-dots__dot"></button>
						<button type="button" class="team-slider-dots__dot"></button>
					</div>
					<button type="button" class="team-slider-btn team-slider-btn_prev"></button>
					<button type="button" class="team-slider-btn team-slider-btn_next"></button>
				</div>
				<div class="our-team__text">
					<h3 class="h3 our-team-title">
						<?= $redux['team-info__title'] ?>
					</h3>
					<?php if(!empty($redux['team-info__description-1'])){ ?>
					<p class="our-team-description">
						<?= $redux['team-info__description-1'] ?>
					</p>
					<?php
					}
					?>
					<?php if(!empty($redux['team-info__description-2'])){ ?>
					<p class="our-team-description">
						<?= $redux['team-info__description-2'] ?>
					</p>
					<?php
					}
					?>
					<a href="<?= get_permalink(41) ?>" class="our-team-link">
						<?php $redux['team-info__link'] ?>
					</a>
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
?>